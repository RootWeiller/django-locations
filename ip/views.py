from decimal import *
import urllib, json
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404
from ip.forms import registerForm
from ip.models import Location
from django.contrib.gis.geoip import GeoIP

def geocode(request):
	if request.method == "POST":
		rform = registerForm(request.POST)
		if rform.is_valid():
			cd = rform.cleaned_data

			import urllib2
			g = GeoIP()
			address = urllib2.urlopen('http://ip.42.pl/raw').read()
			ips = g.city(address)
			city = ips['country_name']
			latitude = ips['latitude']
			longitude = ips['longitude']
			country = ips['country_code']
			postal_code = ips['postal_code']
			data = Location(
				country = cd['country'],
				state = cd['state'],
				address = address,
		   		city = city,
		   		latitude = latitude,
		   		longitude = longitude,
			)
   			data.save()
    		return render_to_response('home.html')
	else:
		rform = registerForm()
	return render_to_response('status_set.html',{'rform':rform},context_instance=RequestContext(request)) 

